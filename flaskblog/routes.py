from flask import render_template, url_for, flash, redirect
from flaskblog.forms import RegistrationForm, LoginForm, JoinProjectForm
from flaskblog import app, db, bcrypt
from flaskblog.models import User, Project, Project_members
from flask_login import login_user, current_user, logout_user, login_required


@app.route('/home')
@app.route('/Home')
@app.route('/')
def home():
    return render_template('home.html')

@app.route("/about")
@app.route("/About")
def about():
    return render_template('about.html', title='IDP About Page')


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created. You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register',form=form)


@app.route('/login',methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            return redirect(url_for('home'))
        else:
            flash('login unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Log in',form = form)


@app.route('/Log out')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/JoinProject', methods=['GET','POST'])
@login_required
def join_project(): #there has to be validation function here that checks whether the number of people in a project is at it's limit or not
    # if current_user.is_authenticated:
    #     return redirect(url_for('home'))

    projectForm = JoinProjectForm()
    loginForm = LoginForm()
    members=Project_members.query.all() #This might need tweaking

    if projectForm.validate_on_submit():
        member = Project_members\
            (current_project=projectForm.project_titles.data, member_matric=projectForm.matric_no.data)#member needs to be added to the database
        db.session.add(member)
        db.session.commit()
        flash('You are now a member in this group', 'success')
        return render_template('IOT_based_inverter.html',members=members)
    else:
        flash('This project cannot be taken now')
    return render_template('join_project.html', title='Join a Project', members=members, form=projectForm)

@app.route('/Project Homepage')
def project_homepage():
    return render_template('project_homepage.html')
