from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from wtforms_sqlalchemy.fields import QuerySelectField
from flaskblog.models import User, Project, Project_members
from flaskblog import db


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2,max=20)])
    email = StringField('Email', validators=[DataRequired(),Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign up')

    def validate_username(self,username):
        user = User.query.filter_by(username=username.data).first()#if there is no user, the value will be 'none'

        if user:
            raise ValidationError('That username is taken. Please choose a different one')

    def validate_email(self,email):
        email = User.query.filter_by(email=email.data).first()#if there is no email, the value will be 'none'

        if email:
            raise ValidationError('This email already has an account')


class LoginForm(FlaskForm):
    username = StringField('Username')
    email = StringField('Email', validators=[DataRequired(),Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Log in')


#the max number of people here will be 5
#should display all project names in choices
#should display all fellow members in the project.(THIS WILL BE IN THE HTML, NOT HERE)
#should access members of project

# def projectChoice_query():
#     # return Project.query(Project).all() #This might need tweaking
#     return db.session.query(Project).all()

class JoinProjectForm(FlaskForm):
    # project_titles = QuerySelectField(allow_blank=False, query_factory=projectChoice_query, validators=[DataRequired()])
    project_titles = SelectField\
        ('Project', choices=[('1','An IOT based smart Micro inverter'),('2', 'Dynamic Wireless Charging of Electric Vehicles '),('3','EMF Detector App')])

    matric_no = StringField('Matric', validators=[DataRequired()])
    submit = SubmitField('Join this project')
    #the following method will check if member number in project is maximum,whether the user has
    def validate_member(self, Matric_No, member_matric):
        presentMembers = Project_members.query.order_by(member_matric=Matric_No.data).all()#This might need tweaking. Not sure if 'presentMembers' can have multiple data
        member = Project_members.query.filter_by(member_matric=Matric_No.data).first()#this will check if the member already is in the project title

        if len(presentMembers) > 5:
            raise ValidationError('This title has maximum number of participants')

        if member:
            raise ValidationError('You are already working on this title')


    def __repr__(self):
        return f"JoinProjectForm('{self.project_titles}')"