from flaskblog import db, login_manager
from datetime import datetime
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


#Users might not have Projects
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    # current_project = db.Column(db.Integer,db.ForeignKey('project.id'), nullable=False) #this links User to Project
    # post = db.relationship('Post', backref='author', lazy=True)
    def __repr__(self):
        return f"User('{self.username}','{self.email}','{self.image_file}')"


# class Post(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     title = db.Column(db.String(100), nullable=False)
#     date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
#     content = db.Column(db.Text, nullable=False)
#     user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
#     def __repr__(self):
#         return f"post('{self.title}','{self.date_posted}')"



class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(300), unique=True, nullable=False)
    members = db.relationship('Project_members', backref='project_title', lazy=True)   #this won't be a column in the table

    def __repr__(self):
        return f"Project('{self.title}')"


#project_members will have a project
class Project_members(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # member_name = db.Column(db.String(20), unique=False, nullable=False)
    # member_email = db.Column(db.String(120), unique=True, nullable=False)
    member_matric = db.Column(db.String(9), unique=True, nullable=False)
    current_project = db.Column(db.Integer, db.ForeignKey('project.id'), nullable=False)

    def __repr__(self):
        return f"Project_members('{self.member_matric}','{self.current_project}')"

    # def __repr__(self):
    #     return f"Project_members('{self.member_matric}')"